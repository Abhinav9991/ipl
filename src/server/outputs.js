const writeTo = require("../server/wtiteTo");
const ipl = require("../server/ipl");

const matchesPath = "../data/matches.csv"
const deliveriesPath = "../data/deliveries.csv";

const csvToJson = require("csvtojson");

csvToJson()
    .fromFile(matchesPath)
    .then((matches) => {
        csvToJson()
            .fromFile(deliveriesPath)
            .then((deliveries) => {
                const matchesPerYearPerTeam = ipl.matchesPerYear(matches);
                const filePath1 = "../../src/public/output/matchesPerYear.json";
                writeTo(filePath1, matchesPerYearPerTeam);

                const matchesWon = ipl.matchesWon(matches);
                const filePath2 = "../../src/public/output/matchesWon.json";
                writeTo(filePath2, matchesWon);

                const extra_runs = ipl.extrasIn2016(matches, deliveries);
                const filePath3 = "../../src/public/output/extraRuns.json";
                writeTo(filePath3, extra_runs);

                const economy = ipl.economicalBowlers(matches, deliveries);
                const filePath4 = "../../src/public/output/economy.json";
                writeTo(filePath4, economy);

                const tossMatch=ipl.winsTossAndMatches(matches);
                const filePath5="../../src/public/output/tossmatch.json";
                writeTo(filePath5,tossMatch);

                const POTM=ipl.highestPOTM(matches);
                const filePath6="../../src/public/output/potm.json";
                writeTo(filePath6,POTM);

                const bestEconomyBowler=ipl.bestBowler(deliveries);
                const filePath7="../../src/public/output/bestEconomyBowler.json";
                writeTo(filePath7,bestEconomyBowler);

                const dismisals=ipl.dismisals(deliveries);
                const filePath8="../../src/public/output/dismissals.json";
                writeTo(filePath8,dismisals);

                const strikeRate=ipl.strikeRate(deliveries,matches)
                const filePath9="../../src/public/output/strikeRate.json";
                writeTo(filePath9,strikeRate);
                
            });
    });