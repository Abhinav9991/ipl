
// matches per team per year
const matchesPerYear = (matches) => {
    let matchIpl = {}
    for (let index = 0; index < matches.length; index++) {
        if (matchIpl.hasOwnProperty(matches[index].season)) {

            matchIpl[matches[index].season] += 1
        } else {

            matchIpl[matches[index].season] = 1
        }
    }
    return matchIpl
}

// Number of matches won per team per year in IPL.
const matchesWon = (matches) => {
    let won = {}
    for (let index = 0; index < matches.length; index++) {

        if (won.hasOwnProperty(matches[index].winner)) {

            if (won[matches[index].winner].hasOwnProperty(matches[index].season)) {
                won[matches[index].winner][matches[index].season] += 1

            } else {
                won[matches[index].winner][matches[index].season] = 1
            }
        }
        else {
            if (matches[index].winner !== "") {

                won[matches[index].winner] = {};
                won[matches[index].winner][matches[index].season] = 1
            }
        }

    }
    return won;
}

// Extra runs conceded per team in the year 2016
const extrasIn2016 = (matches, deliveries) => {

    let id2016 = matches.filter((match) => match.season == 2016)
        .map((match) => parseInt(match.id));
    // console.log(id2016);

    let extras = {}
    for (let index = 0; index < deliveries.length; index++) {

        if (id2016.includes(parseInt(deliveries[index].match_id))) {

            if (extras.hasOwnProperty(deliveries[index].bowling_team)) {

                extras[deliveries[index].bowling_team] += parseInt(deliveries[index].extra_runs)
            }
            else {
                extras[deliveries[index].bowling_team] = parseInt(deliveries[index].extra_runs)
            }
        }
    }
    return extras
}
// console.log(extrasIn2016(matches,deliveries));

// Top 10 economical bowlers in the year 2015

const economicalBowlers = (matches, deliveries) => {
    let id2015 = matches.filter((match) => match.season == 2015)
        .map((match) => parseInt(match.id));

    let economy = {}
    for (let index = 0; index < deliveries.length; index++) {
        if (id2015.includes(parseInt(deliveries[index].match_id))) {
            if (economy.hasOwnProperty(deliveries[index].bowler)) {
                economy[deliveries[index].bowler].runs += parseInt(deliveries[index].total_runs)
                economy[deliveries[index].bowler].balls += 1
                let runs = economy[deliveries[index].bowler].runs
                let balls = economy[deliveries[index].bowler].balls
                economy[deliveries[index].bowler].economy = (runs / (balls / 6)).toFixed(2)
            }
            else {
                economy[deliveries[index].bowler] = { "runs": parseInt(deliveries[index].total_runs), "balls": 1, "economy": 0 }
            }
        }

    }


    const economyArray = {};
    for (let bowlerName in economy) {
        economyArray[bowlerName] = economy[bowlerName].economy
    }

    const sortedEconomy = Object.entries(economyArray)
        .sort(([, value1], [, value2]) => value1 - value2)
        .reduce((obj, [key, value]) => ({
            ...obj,
            [key]: value
        }), {})

    const topBowlers = Object.keys(sortedEconomy).slice(0, 10).reduce((result, key) => {
        result[key] = sortedEconomy[key];

        return result;
    }, {});

    return topBowlers



}

// Find the number of times each team won the toss and also won the match
const winsTossAndMatches = (matches) => {
    let winnersArray = matches.reduce((accumulator, current) => {
        if (!current.id) {
            return accumulator;
        } else {
            if (current.winner === current.toss_winner) {
                if (accumulator[current.winner]) {
                    accumulator[current.winner]++;
                } else {
                    accumulator[current.winner] = 1;
                }
            }
        }
        return accumulator;
    }, {});
    return winnersArray
}



const highestPOTM = (matches) => {
    let potm = matches.reduce((accumulator, current) => {
        if (accumulator.hasOwnProperty(current.season)) {
            if (accumulator[current.season].hasOwnProperty(current.player_of_match)) {
                accumulator[current.season][current.player_of_match] += 1
            }
            else {
                accumulator[current.season][current.player_of_match] = 1
            }

        } else {
            accumulator[current.season] = {}
        }
        return accumulator
    }, {})

    let sortedOrderOfAwards = Object.values(potm)
        .map(nameAndNoOfAwards => Object.entries(nameAndNoOfAwards)
            .sort((current, previous) => current[1] - previous[1]));
    const topPlayers = sortedOrderOfAwards.map(nameAwards => nameAwards[nameAwards.length - 1])


    let mostAwardsPerYear = matches.reduce((mostAwardsPerYear, match) => {
        mostAwardsPerYear[match.season] = 0;
        return mostAwardsPerYear;
    }, {})

    Object.keys(mostAwardsPerYear).map((element, index) => {
        {
            let name = topPlayers[index][0];
            let value = topPlayers[index][1];
            mostAwardsPerYear[element] = { [name]: value }
        }
    })

    return mostAwardsPerYear;

}
// Find the strike rate of a batsman for each season

const strikeRate = (deliveries, matches) => {
    const yearWiseData = (matchID) => {
        let yearObj = deliveries.filter((eachObj) => matchID.includes(eachObj["match_id"]))
        let batsmanArray = [];
        for (let obj of yearObj) {
            if (batsmanArray.includes(obj['batsman'])) {
                continue;
            }
            else {
                batsmanArray.push(obj['batsman'])
            }
        }
        let batsmanObj = {}
        for (let batsman of batsmanArray) {
            let runs = 0
            let balls = 0
            for (let obj of yearObj) {
                if (batsman == obj['batsman']) {
                    runs += Number(obj['batsman_runs'])
                    if (obj['wide_runs'] == 0) {
                        balls += 1
                    }
                }
            }
            batsmanObj[batsman] = ((runs / balls) * 100).toFixed(2);
        }

        return batsmanObj
    }
    const seasonAndID = matches.reduce((group, current) => {
        if (group.hasOwnProperty(current.season)) {
            group[current.season].push(current.id)

        }
        else {
            group[current.season] = [current.id]
        }
        return group
    }, {})
    seasonAndIDArray = Object.entries(seasonAndID)
    let seasonData = {}
    for (let i = 0; i < seasonAndIDArray.length; i++) {
        seasonData[seasonAndIDArray[i][0]] = yearWiseData(seasonAndIDArray[i][1])
    }
    return seasonData


}



// Find the bowler with the best economy in super overs

const bestBowler = (deliveries) => {
    let economyOfBowlers = deliveries.reduce((economyBowlers, match) => {

        if (economyBowlers.hasOwnProperty(match.bowler)) {

            if (match.is_super_over != "0") {
                if (match.wide_runs === "0" && match.noball_runs) {
                    economyBowlers[match.bowler].balls += 1;
                }
                economyBowlers[match.bowler].runs += parseInt(match.total_runs);
                let run = economyBowlers[match.bowler].runs;
                let ball = economyBowlers[match.bowler].balls;
                economyBowlers[match.bowler].economy = (run / (ball / 6)).toFixed(2);
            }
        }
        else {
            if (match.is_super_over != "0") {

                if (match.wide_runs === "0" && match.noball_runs) {
                    economyBowlers[match.bowler] = { "runs": parseInt(match.total_runs), "balls": 1, "economy": 0 };
                }

            }
        }
        return economyBowlers;
    }, {})
    // return economyOfBowlers

    let economyOnly = Object.values(economyOfBowlers).map((valuesOfEconomyBowlers) => {
        return parseFloat(valuesOfEconomyBowlers.economy)
    });

    let sortedEconomy = economyOnly.sort((first, second) => {
        return first - second
    })

    const topEconomy = {};

    Object.keys(economyOfBowlers).filter((bowlers) => {
        if (economyOfBowlers[bowlers]["economy"] == sortedEconomy[0]) {
            return topEconomy[bowlers] = sortedEconomy[0]
        }

    })


    return topEconomy
}
// Find the highest number of times one player has been dismissed by another player

const dismisals = (deliveries) => {
    const highestDismisals = deliveries.reduce((dismis, current) => {
        if (dismis.hasOwnProperty(current.bowler)) {
            if (current.player_dismissed.length != 0) {
                if (dismis[current.bowler][current.player_dismissed]) {
                    dismis[current.bowler][current.player_dismissed].time += 1
                }
            }
            else {
                if (current.player_dismissed.length != 0) {
                    if (current.player_dismissed.length != 0) {

                        dismis[current.bowler][current.player_dismissed] += 1
                    }
                }
            }
        }
        else {
            if (current.player_dismissed.length != 0) {
                dismis[current.bowler] = {}
                dismis[current.bowler][current.player_dismissed] = 1
            }
        }
        return dismis
    }, {})
    return highestDismisals
}




module.exports = { matchesPerYear, matchesWon, extrasIn2016, economicalBowlers, winsTossAndMatches, highestPOTM, bestBowler, dismisals, strikeRate }